//
//  IOLibrary.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class IOLibrary
{
  
  // read a line from console and trim white chars at both ends
  class func getConsoleInputLine() -> String
  {
    let keyboard = FileHandle.standardInput
    let inputData = keyboard.availableData
    let rawString = NSString(data: inputData, encoding:String.Encoding.utf8.rawValue)
    if let string = rawString {
      return string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    } else {
      return "Invalid input"
    }
  }
  
  // save an array of Strings into a file in the given folder and file
  // !! the subfolder in "datafiles" must exist !!
  class func saveStringArray(data: [String], toFolder: String, andFile: String)
  {
    let fullPath:String! = NSHomeDirectory() + "/workspace-swift/datafiles" + "/" + toFolder + "/" + andFile
    let joined = data.joined(separator: "\n")
    //////let joined = "\n".join(data)
    do
    {
      try joined.write(toFile: fullPath, atomically: false, encoding: String.Encoding.utf8);
    }
    catch
    {
      print("could not save String array to a file\n")
    }
  }
  
  // getFileContentAsStringFrom
  class func getFileContentAsStringFrom(folder: String, andFile: String) -> String
  {
    var fileContent:String? = ""
    let fullPath:String! = NSHomeDirectory() + "/workspace-swift/datafiles" + "/" + folder + "/" + andFile
    
    do {
      fileContent = try String(contentsOfFile: fullPath, encoding: String.Encoding.utf8)
      //print("\(readFile)")
      // the above prints "some text"
    } catch let error as NSError
    {
      print("Error: \(error)")
    }
    
    let str:String = fileContent! as String
    
    return str
  }
  
  
  // get array of Strings from a given folder and file
  class func getStringArrayFrom(folder: String, andFile: String) -> [String]
  {
    var data: [String] = ["No File Content !!!"]
    var fileContent:String? = ""
    let fullPath:String! = NSHomeDirectory() + "/workspace-swift/datafiles" + "/" + folder + "/" + andFile
    
    do {
      fileContent = try String(contentsOfFile: fullPath, encoding: String.Encoding.utf8)
      //print("\(readFile)")
      // the above prints "some text"
    } catch let error as NSError
    {
      print("Error: \(error)")
    }
    
    if let content = fileContent
    {
      data = content.components(separatedBy: NSCharacterSet.newlines)
      //println(fileContent)
      //println(data)
    }
    return data
  }
  
  // list all files and folders in the given folder (subfolder of /workspace-swift/datafiles)
  class func listAllFilesAndFoldersIn(folder: String)
  {
    let fullPath:String! = "/workspace-swift/datafiles/" + folder
    let fileManager = FileManager.default
    let enumerator = fileManager.enumerator(atPath: NSHomeDirectory() + fullPath)
    
    while let element = enumerator!.nextObject() as? String
    {
      print(element, separator: "", terminator: "\n")
      //println(element)
      //if element.hasSuffix("ext")
      //{ // checks the extension
      // }
    }
  }
  
  // convert a String to Int
  class func getIntFromString(string: String?) -> Int
  {
    let intNum:Int = Int(string!)!
    return intNum;
  }
  
  // get Int from console line (i.e. one Int on the line)
  class func getIntFromConsole() -> Int
  {
    let line:String = getConsoleInputLine()
    return Int(line)!
    ////return self.getIntFromString(string: line)
  }
  
  // convert a String to Double
  class func getDoubleFromString(string : String?) -> Double
  {
    let doubleNum:Double! = NumberFormatter().number(from: string!)?.doubleValue
    return doubleNum;
  }
  
  // get Double from console line (i.e. one Double per the line
  class func getDoubleFromConsole() -> Double
  {
    let line:String = getConsoleInputLine()
    return getDoubleFromString(string: line)
    ////return self.getDoubleFromString(string: line)
  }
  
}
