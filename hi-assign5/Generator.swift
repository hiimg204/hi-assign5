//
//  Generator.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

public class Generator {
  
  public static let firstNames: [String] = ["Alex", "Ben", "Cecile", "Dan",
                                            "Eva", "Felix", "Gomez", "Hanna",
                                            "Ivan"]
  
  public static let lastNames: [String] = ["Atol", "Bundle", "Circus", "Dome", "Eternal",
                                           "Fantomas", "Giant", "Humphrey", "Immense"]
  
  public static let streets:[String] = ["Albatros", "Buenos", "Calamity", "Desolation",
                                        "Ecstatic", "Fanfare", "Gologo", "Hovnivar", "Icarus"]
  
  public static let cities: [String] = ["Comox", "Courtenay", "Cumberland",
                                        "Campbell River", "Nanaimo", "Port Hardy", "Port Alberni", "Plosina", "Victoria"]
  
  public static let professions: [String] = ["FullTime Student", "PartTime Student",
                                             "Instructor", "PartTime Instructor", "Administrator", "Janitor",
                                             "International Student", "Librarian", "Office Worker"];
  
  public class func getFirstName() -> String
  {
    let count:Int = firstNames.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return firstNames[rindex]
  }
  
  public class func getLastName() -> String
  {
    let count:Int = lastNames.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return lastNames[rindex]
  }
  
  public class func getBirthYear() -> String
  {
    let count:Int = 80;
    let year:Int = 1920 + Int(arc4random()) % count
    
    let strYear:String = String(format: "%i", year)
    return strYear;
  }
  
  public class func getStreet() -> String
  {
    let count:Int = streets.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return streets[rindex]
  }
  
  public class func getStreetNumber() -> String
  {
    let streetNumber:Int = Int(arc4random()) % 10000
    let strStreetNumber:String = String(format: "%i", streetNumber)
    return strStreetNumber;
  }
  
  public class func getCity() -> String
  {
    let count:Int = cities.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return cities[rindex]
  }
  
  public class func getProvince() -> String
  {
    let provinces: [String] = ["AB", "BC", "ON", "MN", "PE", "SK", "NS", "NL"]
    
    let count:Int = provinces.count
    let rindex:Int = Int(arc4random()) % count;
    return provinces[rindex];
  }
  
  public static func getProfession() -> String
  {
    let count:Int = professions.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return professions[rindex]
  }
  
  
  public static func md5(string:String) -> String!
  {
    let str = string.cString(using: String.Encoding.utf8)
    let strLen = CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8))
    let digestLen = Int(CC_MD5_DIGEST_LENGTH)
    let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
    
    CC_MD5(str!, strLen, result)
    
    let hash = NSMutableString()
    for i in 0..<digestLen
    {
      hash.appendFormat("%02x", result[i])
    }
    
    result.deallocate(capacity: digestLen)
    return String(format: hash as String)
  }
  
  public class func generatePersonRecord() -> String
  {
    let firstName:String = Generator.getFirstName()
    let lastName:String = Generator.getLastName()
    let birthYear:String = Generator.getBirthYear()
    let street:String = Generator.getStreet()
    let streetNumber:String = Generator.getStreetNumber()
    let city:String = Generator.getCity()
    let province:String = Generator.getProvince()
    
    // create a combined string to calculate MD5
    var totalString:String = ""
    totalString += firstName
    totalString += lastName
    totalString += birthYear
    totalString += street
    totalString += streetNumber
    totalString += city
    totalString += province
    
    let numberOfGrades = Int(arc4random_uniform(15))
    var strGrades:String = ""
    for _ in 0...numberOfGrades
    {
      let grade = String(arc4random_uniform(50) + 50)
      strGrades = strGrades +  "|\(grade)"
      
    }
    
    let md5String:String = Generator.md5(string: firstName + lastName + birthYear)
    
    let strRecord:String = String(format:"%@|%@|%@|%@|%@|%@|%@|%@%@", md5String,
                                  firstName, lastName, birthYear, street, streetNumber, city, province, strGrades)
    
    return strRecord;
  }
  
  public class func generateMultiplePersonRecords(howMany:Int) -> [String]
  {
    var array: [String] = []
    
    for _ in 0 ..< howMany
    {
      array.append(Generator.generatePersonRecord())
    }
    return array;
  }
  
  
}
