//
//  Person.swift
//  Problem2
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Person {
  var firstName:String = ""
  var lastName:String = ""
  var birthDay:String = ""
  var city:String = ""
  var province:String = ""
  
  // initializer method
  init(firstName:String, lastName:String, birthDay:String, city:String,
       province:String) {
    self.firstName = firstName
    self.lastName = lastName
    self.birthDay = birthDay
    self.city = city
    self.province = province
  }
  
  // description method
  var description:String {
    let str:String = "\(firstName) \(lastName) \(birthDay) \(city) \(province)"
    return str
  }
}
