//
//  Project.swift
//  Problem2
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

public class Project:CustomStringConvertible {
  
  // array containing all Persons in this Project
  private(set) var people:[Person] = []
  
  // TODO - implement all necessary methods
  
  // initializer method
  init() {
    people = []
  }
  
  // description method
  public var description:String {
    var str:String = ""
    for person in people
    {
      str += person.description
      str += "\n"
    }
    return str
  }
  
  // add a new Person to the Project method
  func addPerson(person1:Person) {
    people.append(person1)
  }
  
  // sort people by age in descending order
  func sortAgeDesc() -> [Person] {
    let sorted = people.sorted(by: {Int($0.birthDay)! < Int($1.birthDay)!})
    return sorted
  }
  
  // sort people in the ascending order by their first names
  func sortFirstName() -> [Person] {
    let sorted = people.sorted(by: {String($0.firstName)! <
      String($1.firstName)!})
    return sorted
  }
  
  // method that uses "filter" to return only peple who are between
  // 25 and 40 years old
  func filterAge() -> [Person] {
    let sorted = people.filter{(Int($0.birthDay)! <= 1991) &&
      (Int($0.birthDay)! > 1976)}
    return sorted
  }
  
  // methods that returns a tuple containing the  youngest and oldest
  // persons in the project team
  func maxMinAge() -> (youngest:String, oldest:String) {
    //      var youngest: Person = people[0]
    //      var oldest: Person = people[0]
    //
    //      for person in people {
    //        if person.birthDay > youngest.birthDay {
    //          youngest = person
    //        }
    //        if person.birthDay < oldest.birthDay {
    //          oldest = person
    //        }
    //      }
    //      return (youngest.description,oldest.description)
    
    let youngest = people.max(by: {Int($0.birthDay)! < Int($1.birthDay)!})
    let oldest = people.min(by: {Int($0.birthDay)! < Int($1.birthDay)!})
    
    return (youngest!.description,oldest!.description)
  }
  
  // method that uses "reduce" to return number of people younger than 22 years
  func reduce22() -> Int {
    
    func combinator(accumulator: Int, current:Person) -> Int {
      var count = accumulator
      if Int(current.birthDay)! > 1994 {
        count += 1
      }
      return count
    }
    
    return people.reduce(0, combinator)
  }
  
  
  // method that uses "reduce" to return the average age of the team
  func reduceAverageAge() -> Int {
    
    func sumAge(info: (age:Int,totalPerson:Int), current:Person) -> (Int,Int) {
      var result = info
      
      result.age += ( Int(current.birthDay)! )
      result.totalPerson += 1
      
      return result
    }
    
    let aux: (age:Int,totalPerson:Int) = people.reduce((0,0), sumAge)
    
    let result = aux.age / aux.totalPerson
    
    return result
  }
  
  // method that uses "reduce" to return the average length of the last
  // names in the team (number of characters)
  func averageLastName() -> Int {
    func averageLastNameLength(result: (numberLetters:Int,numberNames:Int),
                               current:Person) -> (Int,Int) {
      var aux = result
      
      aux.numberLetters += current.lastName.characters.count
      aux.numberNames += 1
      
      return aux
    }
    
    let aux: (numberLetters:Int,numberNames:Int) =
      people.reduce((0,0),averageLastNameLength)
    
    return aux.numberLetters / aux.numberNames
  }
  
  
}
