//
//  main.swift
//  Problem2
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

// create an object from the Project class
let myProject: Project = Project()

// use the Generator class to create array of Persons
let HOW_MANY: Int = 100

let strArray = Generator.generateMultiplePersonRecords(howMany: HOW_MANY)
for aString in strArray {
  var strTmp = aString.components(separatedBy: "|")
  let tmpPerson = Person(firstName: strTmp[1], lastName: strTmp[2],
                         birthDay: strTmp[3], city: strTmp[4],
                         province: strTmp[6])
  myProject.addPerson(person1:tmpPerson)
}

//print out data inside myProject
print(myProject.description)

//using for to display results
print("\nSort by age descending")
for ageDesc in myProject.sortAgeDesc() {
  print(ageDesc.description)
}

print("\nSort by Name ascending")
for nameAsc in myProject.sortFirstName() {
  print(nameAsc.description)
}

print("\nFilter Age between 25 and 40 Years Old")
for age in myProject.filterAge() {
  print(age.description)
}

print("\nCreate A Tuple")
print(myProject.maxMinAge())

print("\nTotal Under 22: \(myProject.reduce22())")

let averAge = 2016 - myProject.reduceAverageAge()
print("\nAverage Age: \(averAge)")

print("\nAverage Last Name Size: \(myProject.averageLastName())")
