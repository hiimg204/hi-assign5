//
//  Magazine.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Magazine: ReadingMaterial {
  let topic: String
  let date: String
  let number: Int
  
  init(title: String, type: ReadingMaterialType, pages:Int, author:String,
       isbn: String, topic:String, date:String, number:Int) {
    self.topic = topic
    self.date = date
    self.number = number
    super.init(title: title, type: type, pages: pages, author: author,
               isbn: isbn)
  }
  
  override var description:String {
    let aux:String = "\nTopic: \(topic)\nDate: \(date)\n" +
    "Number: \(number)"
    return super.description + aux
  }
}
