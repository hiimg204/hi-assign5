//
//  Journal.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Journal: ReadingMaterial {
  let topic: String
  let edition: Int
  let date: String
  
  init(title: String, type: ReadingMaterialType, pages:Int, author:String,
       isbn: String, topic:String, edition:Int, date:String) {
    self.topic = topic
    self.edition = edition
    self.date = date
    super.init(title: title, type: type, pages: pages, author: author,
               isbn: isbn)
  }
  
  override var description:String {
    let aux:String = "\nTopic: \(topic)\nEdition: \(edition)\nPublication " +
    "date: \(date)"
    return super.description + aux
  }
}
