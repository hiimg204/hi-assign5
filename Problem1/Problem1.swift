//
//  Problem1.swift
//  Problem1
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//
// Project:      assign-5-2016
// File:         Problem1.swift
// Author:       Ismael F. Hepp
// Date:         Oct 25, 2016
// Course:       IMG204
//
// Problem Statement - Class Inheritance :
//
// Design and implement a set of classes that define various types of reading
// material: books, novels, magazines, technical journals, textbooks, and so
// on. Be sure that appropriate classes are created by using class inheritance.
//
// Include data values that describe various attributes (properties) of the
// material, such as the number of pages and the names of the primary
// characters.
//
// Include appropriate overridden methods in your classes so that you can
// print relevant information about each object. Your main.swift should
// instantiate and test all methods of at least one object of each class.
//
// Inputs:   none
// Outputs:  information about each object created in main.swift

import Foundation

public class Problem1 {
  func run() {
    
    let publication: ReadingMaterial
    publication = ReadingMaterial(title: "Example 1",
                                  type: .ReadingMaterial,
                                  pages: 100,
                                  author: "John Example",
                                  isbn: "0000000000001")
    
    print(publication.description)
    
    let publication2: Book
    publication2 = Book(title: "Example 2", type: .Book, pages: 120,
                        author: "Joan Example", isbn: "0000000002",
                        genre: "Horror")
    
    print(publication2.description)
    
    let publication3: Journal
    publication3 = Journal(title: "Example 3", type: .Journal, pages: 100,
                           author: "Dr. Example", isbn: "0000000003",
                           topic: "Example Science", edition: 3,
                           date: "10/10/2016")
    
    print(publication3.description)
    
    let publication4: Magazine
    publication4 = Magazine(title: "Example 04", type: .Magazine,
                            pages: 50, author: "Marie Example",
                            isbn: "00000004", topic: "Latest Examples",
                            date: "11/11/2016", number: 50)
    
    print(publication4.description)
    
    let publication5: Newspaper
    publication5 = Newspaper(title: "Daily Example", type: .Newspaper,
                             pages: 40, author: "ENN", isbn: "000005",
                             date: "12/12/2016")
    
    print(publication5.description)
    
    let publication6: Novel
    publication6 = Novel(title: "1001 Examples", type: .Novel, pages: 250,
                         author: "Arthur E. Xample", isbn: "00000006",
                         genre: "Adventure", chapters: 50)
    
    print(publication6.description)
    
    
    
    
    
    
    
  }
  
}

