//
//  Book.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Book: ReadingMaterial {
  let genre: String
  
  init(title: String, type: ReadingMaterialType, pages:Int, author:String,
       isbn: String, genre:String) {
    self.genre = genre
    super.init(title: title, type: type, pages: pages, author: author, isbn: isbn)
  }
  
  override var description:String {
    let aux:String = "\nGenre: \(genre)"
    return super.description + aux
  }
}
