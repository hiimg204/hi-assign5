//
//  Newspaper.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Newspaper: ReadingMaterial {
  let date: String
  
  init(title: String, type: ReadingMaterialType, pages:Int, author:String,
       isbn: String, date:String) {
    self.date = date
    super.init(title: title, type: type, pages: pages, author: author,
               isbn: isbn)
  }
  
  override var description:String {
    let aux:String = "\nDate: \(date)"
    return super.description + aux
  }
}
