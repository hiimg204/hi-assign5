//
//  ReadingMaterialType.swift
//  hi-assign5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

enum ReadingMaterialType: String {
  case ReadingMaterial = "Reading Material", Book = "Book",
  Journal = "Journal", Newspaper = "Newspaper", Magazine = "Magazine",
  Novel = "Novel"
}
