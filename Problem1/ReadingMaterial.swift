//
//  ReadingMaterial.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class ReadingMaterial: CustomStringConvertible {
  let title: String
  let pages: Int
  let author: String
  let isbn: String
  let type: ReadingMaterialType
  
  init(title: String, type: ReadingMaterialType, pages:Int, author:String,
       isbn: String) {
    self.title = title
    self.type = type
    self.pages = pages
    self.author = author
    self.isbn = isbn
  }
  
  var description:String {
    return "Title: \(title)\nReading material type: \(type.rawValue)\n" +
    "Author: \(author)\nPages: \(pages)\nISBN: \(isbn)"
  }
}
