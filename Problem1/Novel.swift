//
//  Novel.swift
//  hi-assign5
//
//  Created by Student on 2016-10-22.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Novel: ReadingMaterial {
  let genre: String
  let chapters: Int
  
  init(title: String, type: ReadingMaterialType, pages:Int, author:String,
       isbn: String, genre:String, chapters: Int) {
    self.genre = genre
    self.chapters = chapters
    super.init(title: title, type: type, pages: pages, author: author,
               isbn: isbn)
  }
  
  override var description:String {
    let aux:String = "\nGenre: \(genre)\nChapters: \(chapters)"
    return super.description + aux
  }
}
